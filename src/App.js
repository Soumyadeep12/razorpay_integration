import React from 'react';
import axios from "axios";
class App extends React.Component {
  constructor(props){
    super(props);
    this.state={
      amount:0
    }
    this.handleChange = this.handleChange.bind(this);
    this.openPayModal = this.openPayModal.bind(this);
  }
componentDidMount () {
    const script = document.createElement("script");
    script.src = "https://checkout.razorpay.com/v1/checkout.js";
    script.async = true;
    document.body.appendChild(script);
}
handleChange(evt){
    this.setState({
      amount:evt.target.value
    })
  }
openPayModal(amt){
    var amount = amt * 100; //Razorpay consider the amount in paise
    var options = {
      "key": "rzp_test_B0cis88DhePTXp",
      "amount": 0, // 2000 paise = INR 20, amount in paisa
      "name": "Soumy",
      "description": "Integration test",
      'order_id':"",
      "handler": function(response) {
        alert("Success");
        alert(response.razorpay_payment_id);
				alert(response.razorpay_order_id);
				alert(response.razorpay_signature);
      },
      "prefill":{
          "name":'Soumyadeep',
          "email":'abc@gmail.com',
          "contact":'9821702107',
      },
      "theme": {
        "color": "#528ff0"
      }
    };
axios.post('http://localhost:5000/razorpay/order',{amount:amount})
    .then(res=>{
        options.order_id = res.data.id;
        options.amount = res.data.amount;
        console.log(options);
        var rzp1 = new window.Razorpay(options);
        rzp1.open();
    })
    .catch(e=>console.log(e))
    
};
render(){
    return (
    <div>
      Enter the amount: <input type="number" name="amount" onChange={this.handleChange}/><br />
      <button className="btn btn-primary" onClick={(e)=>{this.openPayModal(this.state.amount)}}>Pay Now</button>
    </div>
  );
}
  
}
export default App;